<?php

	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus();
	}

	if ( function_exists( 'add_theme_support' ) ) { 
	  add_theme_support( 'post-thumbnails' ); 
	}

	# Sidebar 1
	if ( function_exists('register_sidebar') )
	    register_sidebar(array(
		'name' => 'Main Sidebar',
        	'before_title' => '<h2>',
	        'after_title' => '</h2>',
			'before_widget' => '<div class="widget">',
        	'after_widget' => '</div>'
	));

	function is_login_page() {
		return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
	}


	function register_theme() {
		if(!is_admin() || !is_login_page()) {
			wp_register_style('style', get_bloginfo('stylesheet_url'),false,0.1);
			wp_register_script('jqtools', 'http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js');
			if (!is_admin()) {
				wp_enqueue_style('style');
			}
		}
	}
	add_action('wp_enqueue_scripts', 'register_theme');

	
?>